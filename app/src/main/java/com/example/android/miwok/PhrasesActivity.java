package com.example.android.miwok;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class PhrasesActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.word_list);

        // array list containing the list of phrase words (used as data source for WordAdapter)
        ArrayList<Word> phraseWordsList = new ArrayList<Word>() {
            { add(new Word("minto wuksus", "Where are you going?")); }
            { add(new Word("vtinnә oyaase'nә", "What is your name?")); }
            { add(new Word("oyaaset...", "My name is...")); }
            { add(new Word("michәksәs?", "How are you feeling?")); }
            { add(new Word("kuchi achit", "I’m feeling good.")); }
            { add(new Word("әәnәs'aa?", "Are you coming?")); }
            { add(new Word("hәә’ әәnәm", "Yes, I’m coming.")); }
            { add(new Word("әәnәm", "I’m coming.")); }
            { add(new Word("yoowutis", "Let’s go.")); }
            { add(new Word("әnni'nem", "Come here.")); }
        };

        // initialize the custom Adapter and pass the context and data source
        WordAdapter wordAdapter = new WordAdapter(this, phraseWordsList, R.color.category_phrases);
        // get the listview from the XML
        // word_list.xml is the XML file containing the ListView
        ListView listView = (ListView) findViewById(R.id.word_list);
        // set the custom Adapter on the ListView
        listView.setAdapter(wordAdapter);
    }
}