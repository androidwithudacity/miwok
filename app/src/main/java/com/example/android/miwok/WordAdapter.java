package com.example.android.miwok;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;

/**
 * Created by Srishti.Gupta on 29-01-2018.
 */

// custom Adapter class extending ArrayAdapter<custom Class>
class WordAdapter extends ArrayAdapter<Word> {

    private int mBackgroundColorResourceId; // background color resource id for all the words in an activity

    /**
     * Constructor
     * @param context: current context used to inflate the layout file
     * @param wordsArrList: data source of objects to be displayed in the ListView
     */
    public WordAdapter(Activity context, ArrayList<Word> wordsArrList, int activityColorResourceId) {
        super(context, 0, wordsArrList); // call to super, viz. ArrayAdapter
        mBackgroundColorResourceId = activityColorResourceId;
    }

    /**
     * Method to get the view associated with each list item in the ListView
     * @param position: AdapterView position requesting a view
     * @param convertView: recycled view to be used for displaying information
     * @param parent: parent ViewGroup used for inflation
     * @return the view to be used for the position in the AdapterView
     */
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        // Get the object located at specified position in the list
        Word currentWord = getItem(position);

        // Check if a view exists to be reused, otherwise inflate a new view
        View listItemView = convertView;
        // Check if a view exists to be reused, otherwise inflate a new view
        // A view is null especially in case of an activity opened for the first time
        // 'layout_list_item' is the layout XML file defining the view for each list item to be
        // displayed in the ListView
        if (listItemView == null) {
            listItemView = LayoutInflater.from(getContext()).inflate(R.layout.layout_list_item,
                    parent, false);
        }

        // Set the information in each view in the list item defined in the XML layout
        // (here, layout_list_item.xml)
        // using the getters in the custom class to get the information
        TextView miwokTextView = (TextView) listItemView.findViewById(R.id.miwok_textview);
        miwokTextView.setText(currentWord.getmMiwokTranslation());

        TextView defaultTextView = (TextView) listItemView.findViewById(R.id.default_textview);
        defaultTextView.setText(currentWord.getmDefaultTranslation());

        ImageView iconImageView = (ImageView) listItemView.findViewById(R.id.icon_imageview);
        if (currentWord.hasImage()) { // display an image only if the current word has an image associated with it
            iconImageView.setImageResource(currentWord.getmImageResourceId());
            iconImageView.setVisibility(View.VISIBLE); // setting visibility back since the views are getting reused
        } else { // hide the image view
            iconImageView.setVisibility(View.GONE);
        }

        // Set the background color of the translation textview container for all words in the corresponding activity
        LinearLayout translationContainerTextView = (LinearLayout) listItemView.findViewById(R.id.translation_container_textview);
        // Find the color that the resource id maps to
        int color = ContextCompat.getColor(getContext(), mBackgroundColorResourceId);
        translationContainerTextView.setBackgroundColor(color);

        // return the view generated for a list item to be displayed in the ListView
        return listItemView;
    }
}