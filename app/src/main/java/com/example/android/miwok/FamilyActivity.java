package com.example.android.miwok;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;

public class FamilyActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.word_list);

        // array list containing the list of family words (used as data source for WordAdapter)
        ArrayList<Word> familyWordsList = new ArrayList<Word>() {
            { add(new Word("әpә", "father", R.drawable.family_father)); }
            { add(new Word("әṭa", "mother", R.drawable.family_mother)); }
            { add(new Word("angsi", "son", R.drawable.family_son)); }
            { add(new Word("tune", "daughter", R.drawable.family_daughter)); }
            { add(new Word("taachi", "elder brother", R.drawable.family_elder_brother)); }
            { add(new Word("chalitti", "younger brother", R.drawable.family_younger_brother)); }
            { add(new Word("teṭe", "elder sister", R.drawable.family_elder_sister)); }
            { add(new Word("kolliti", "younger sister", R.drawable.family_younger_sister)); }
            { add(new Word("ama", "grandmother", R.drawable.family_grandmother)); }
            { add(new Word("paapa", "grandfather", R.drawable.family_grandfather)); }
        };

        // initialize the custom Adapter and pass the context and data source
        WordAdapter wordAdapter = new WordAdapter(this, familyWordsList, R.color.category_family);
        // get the listview from the XML
        // word_list.xml is the XML file containing the ListView
        ListView listView = (ListView) findViewById(R.id.word_list);
        // set the custom Adapter on the ListView
        listView.setAdapter(wordAdapter);
    }
}