package com.example.android.miwok;

/**
 * Created by Srishti.Gupta on 28-01-2018.
 */

class Word {
    private String mMiwokTranslation; // Miwok translation of a word
    private String mDefaultTranslation; // default translation of a word
    private int mImageResourceId = NO_IMAGE_PROVIDED; // image resource id of a word
    private static final int NO_IMAGE_PROVIDED = -1; // flag to keep track of whether a word has an image

    /**
     * Constructor
     * @param miwokWord: Miwok translation of a word
     * @param defaultWord: default translation of a word
     */
    public Word(String miwokWord, String defaultWord) {
        mMiwokTranslation = miwokWord;
        mDefaultTranslation = defaultWord;
    }

    /**
     * Constructor
     * @param miwokWord: Miwok translation of a word
     * @param defaultWord: default translation of a word
     * @param imageId: image resource id of a word
     */
    public Word(String miwokWord, String defaultWord, int imageId) {
        mMiwokTranslation = miwokWord;
        mDefaultTranslation = defaultWord;
        mImageResourceId = imageId;
    }

    /**
     * Method to get Miwok translation of a word
     * @return Miwok translation
     */
    public String getmMiwokTranslation() {
        return mMiwokTranslation;
    }

    /**
     * Method to get default translation of a word
     * @return default translation
     */
    public String getmDefaultTranslation() {
        return mDefaultTranslation;
    }

    /**
     * Method to get image resource id of a word
     * @return image resource id
     */
    public int getmImageResourceId() { return mImageResourceId; }

    /**
     * Method to check if a word has an image
     * @return TRUE if the word has an image; FALSE otherwise
     */
    public boolean hasImage() {
        return mImageResourceId != NO_IMAGE_PROVIDED;
    }
}