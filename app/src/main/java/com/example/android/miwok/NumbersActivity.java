package com.example.android.miwok;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;

public class NumbersActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.word_list);

        // array list containing the list of number words (used as data source for WordAdapter)
        ArrayList<Word> numWordsList = new ArrayList<Word>() {
            { add(new Word("lutti", "one", R.drawable.number_one)); }
            { add(new Word("otiiko", "two", R.drawable.number_two)); }
            { add(new Word("tolookosu", "three", R.drawable.number_three)); }
            { add(new Word("oyyisa", "four", R.drawable.number_four)); }
            { add(new Word("massokka", "five", R.drawable.number_five)); }
            { add(new Word("temmokka", "six", R.drawable.number_six)); }
            { add(new Word("kenekaku", "seven", R.drawable.number_seven)); }
            { add(new Word("kawinta", "eight", R.drawable.number_eight)); }
            { add(new Word("wo’e", "nine", R.drawable.number_nine)); }
            { add(new Word("na’aacha", "ten", R.drawable.number_ten)); }
        };

        // initialize the custom Adapter and pass the context and data source
        WordAdapter wordAdapter = new WordAdapter(this, numWordsList, R.color.category_numbers);
        // get the listview from the XML
        // word_list.xml is the XML file containing the ListView
        ListView numlistView = (ListView) findViewById(R.id.word_list);
        // set the custom Adapter on the ListView
        numlistView.setAdapter(wordAdapter);
    }
}