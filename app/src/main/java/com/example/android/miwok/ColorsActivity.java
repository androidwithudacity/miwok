package com.example.android.miwok;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;

public class ColorsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.word_list);

        // array list containing the list of color words (used as data source for WordAdapter)
        ArrayList<Word> colorWordsList = new ArrayList<Word>() {
            { add(new Word("weṭeṭṭi", "red", R.drawable.color_red)); }
            { add(new Word("chokokki", "green", R.drawable.color_green)); }
            { add(new Word("ṭakaakki", "brown", R.drawable.color_brown)); }
            { add(new Word("ṭopoppi", "gray", R.drawable.color_gray)); }
            { add(new Word("kululli", "black", R.drawable.color_black)); }
            { add(new Word("kelelli", "white", R.drawable.color_white)); }
            { add(new Word("ṭopiisә", "dusty yellow", R.drawable.color_dusty_yellow)); }
            { add(new Word("chiwiiṭә", "mustard yellow", R.drawable.color_mustard_yellow)); }
        };

        // initialize the custom Adapter and pass the context and data source
        WordAdapter wordAdapter = new WordAdapter(this, colorWordsList, R.color.category_colors);
        // get the listview from the XML
        // word_list.xml is the XML file containing the ListView
        ListView listView = (ListView) findViewById(R.id.word_list);
        // set the custom Adapter on the ListView
        listView.setAdapter(wordAdapter);
    }
}